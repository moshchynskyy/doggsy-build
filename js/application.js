;'use strict';

// preloader
$(window).on('load', function () {
    let $preloader = $('#preloader'),
        $svg_anm = $preloader.find('.preloader__svg');
    $svg_anm.fadeOut();
    $preloader.fadeOut();
});
// eof preloader


$(function () {

// header
    $('#toggleMenu').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass("on");
        $('.menu').addClass('menu-open').slideToggle();
    });
    $(window).on('scroll', function () {
        $(this).scrollTop() >= 200 ? $('#header').addClass('scroll-menu')
            : $('#header').removeClass('scroll-menu');
    });
// eof header

// sliders
    $('#mainSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true
    });
    $('.slick-prev').html('<i class="fas fa-angle-left"></i>');
    $('.slick-next').html('<i class="fas fa-angle-right"></i>');

    $('#testimonialsSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });
// eof sliders

// main page
    const setHeightForCustomColumn = () => {
        const height = $('.column-full-6.column-full-text').height();
        $('.column-full-6.column-full-img').height( height );
    };

    $(window).on('resize', () => {
        $(window).width() < 768 ? setHeightForCustomColumn() : $('.column-full-6.column-full-img').height( 'auto' );
    });

    $(window).width() < 768 ? setHeightForCustomColumn() : $('.column-full-6.column-full-img').height( 'auto' );

// eof main page

// card page
    const toggleDogsInfo = () => {
        $('.card__dogs .more').on('click', function () {
            $(this).parents('.card__dogs-wrap').toggleClass('open');
            $(this).text() === 'Подробнее' ?
                $(this).text('Скрыть') :
                $(this).text('Подробнее');
        });
    };
    toggleDogsInfo();

    const initSitterSliders = () => {
        $('#sitterMainSlider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '#sitterNavSlider'
        });
        $('#sitterNavSlider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '#sitterMainSlider',
            dots: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
        $('.slick-next').html('<i class="fas fa-angle-right"></i>');
    };
    initSitterSliders();
// card page

// search page
    const showAdditionalSearchColumn = () => {
        $('#additionalSearch span').on('click', function(e) {
            e.preventDefault();
            $(this).prev().hasClass('fa-angle-up') ?
                $(this).prev().removeClass('fa-angle-up').addClass('fa-angle-down') :
                $(this).prev().removeClass('fa-angle-down').addClass('fa-angle-up');
            $('.search__column-toggle').slideToggle();
            $(this).text() === 'Расширенный поиск' ?
                $(this).text('Скрыть') :
                $(this).text('Расширенный поиск');
        })
    };
    showAdditionalSearchColumn();


    // jquery-UI for search block
    $( function() {

        $('.search__form select').niceSelect();

        $( ".search__form input.search__input").checkboxradio();

        $( "#priceRange" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#priceInput" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
        });
        $( "#priceInput" ).val( "$" + $( "#priceRange" ).slider( "values", 0 ) +
            " - $" + $( "#priceRange" ).slider( "values", 1 ) );


        let dateFormat = "mm/dd/yy",
            from = $( "#dateFrom" )
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#dateTo" ).datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        const getDate = ( element ) => {
            let date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    // EOF jquery-UI for search block

// eof search page

// register page
    $( function() {
        $('.reg-form select').niceSelect();
        $( ".reg-form input.reg-checkbox").checkboxradio();

        $('#setAddress').on('click', function (e) {
            e.preventDefault();
        })
    });

    const showUploadedImg = () => {
        $('input[type="file"]').on('change', function () {
            console.log( $(this).val() ); // TODO: change default img after uploading
        })
    };
    showUploadedImg();

// eof register page

    // masks for input
    $('#series').mask('AA-000000');
    $('#issue_date').mask('00/00/0000');
    $('#birthday').mask('00/00/0000');

    $('.js-autoheight').on('keyup',function(){
        let height = this.scrollHeight;
        let elHeight = $('.js-autoheight').height();
        console.log(elHeight);
        if (height > elHeight) {
            $(this).css('height','auto');
            $(this).height(this.scrollHeight);
        }
    });

    // content-page
    const animateScroll = () => {
        $(document).ready(function(){
            $(".content-page").on("click", "a", function (e) {
                // e.preventDefault();
                let id  = $(this).attr('href'),
                    top = $(id).offset().top;
                $('body,html').animate({scrollTop: top}, 1500);
            });
        });
    };
    animateScroll();
    // eof content-page
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJhcHBsaWNhdGlvbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyI7J3VzZSBzdHJpY3QnO1xuXG4vLyBwcmVsb2FkZXJcbiQod2luZG93KS5vbignbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICBsZXQgJHByZWxvYWRlciA9ICQoJyNwcmVsb2FkZXInKSxcbiAgICAgICAgJHN2Z19hbm0gPSAkcHJlbG9hZGVyLmZpbmQoJy5wcmVsb2FkZXJfX3N2ZycpO1xuICAgICRzdmdfYW5tLmZhZGVPdXQoKTtcbiAgICAkcHJlbG9hZGVyLmZhZGVPdXQoKTtcbn0pO1xuLy8gZW9mIHByZWxvYWRlclxuXG5cbiQoZnVuY3Rpb24gKCkge1xuXG4vLyBoZWFkZXJcbiAgICAkKCcjdG9nZ2xlTWVudScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcIm9uXCIpO1xuICAgICAgICAkKCcubWVudScpLmFkZENsYXNzKCdtZW51LW9wZW4nKS5zbGlkZVRvZ2dsZSgpO1xuICAgIH0pO1xuICAgICQod2luZG93KS5vbignc2Nyb2xsJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAkKHRoaXMpLnNjcm9sbFRvcCgpID49IDIwMCA/ICQoJyNoZWFkZXInKS5hZGRDbGFzcygnc2Nyb2xsLW1lbnUnKVxuICAgICAgICAgICAgOiAkKCcjaGVhZGVyJykucmVtb3ZlQ2xhc3MoJ3Njcm9sbC1tZW51Jyk7XG4gICAgfSk7XG4vLyBlb2YgaGVhZGVyXG5cbi8vIHNsaWRlcnNcbiAgICAkKCcjbWFpblNsaWRlcicpLnNsaWNrKHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgYXJyb3dzOiB0cnVlXG4gICAgfSk7XG4gICAgJCgnLnNsaWNrLXByZXYnKS5odG1sKCc8aSBjbGFzcz1cImZhcyBmYS1hbmdsZS1sZWZ0XCI+PC9pPicpO1xuICAgICQoJy5zbGljay1uZXh0JykuaHRtbCgnPGkgY2xhc3M9XCJmYXMgZmEtYW5nbGUtcmlnaHRcIj48L2k+Jyk7XG5cbiAgICAkKCcjdGVzdGltb25pYWxzU2xpZGVyJykuc2xpY2soe1xuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgICBkb3RzOiB0cnVlLFxuICAgICAgICBhcnJvd3M6IGZhbHNlXG4gICAgfSk7XG4vLyBlb2Ygc2xpZGVyc1xuXG4vLyBtYWluIHBhZ2VcbiAgICBjb25zdCBzZXRIZWlnaHRGb3JDdXN0b21Db2x1bW4gPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGhlaWdodCA9ICQoJy5jb2x1bW4tZnVsbC02LmNvbHVtbi1mdWxsLXRleHQnKS5oZWlnaHQoKTtcbiAgICAgICAgJCgnLmNvbHVtbi1mdWxsLTYuY29sdW1uLWZ1bGwtaW1nJykuaGVpZ2h0KCBoZWlnaHQgKTtcbiAgICB9O1xuXG4gICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCAoKSA9PiB7XG4gICAgICAgICQod2luZG93KS53aWR0aCgpIDwgNzY4ID8gc2V0SGVpZ2h0Rm9yQ3VzdG9tQ29sdW1uKCkgOiAkKCcuY29sdW1uLWZ1bGwtNi5jb2x1bW4tZnVsbC1pbWcnKS5oZWlnaHQoICdhdXRvJyApO1xuICAgIH0pO1xuXG4gICAgJCh3aW5kb3cpLndpZHRoKCkgPCA3NjggPyBzZXRIZWlnaHRGb3JDdXN0b21Db2x1bW4oKSA6ICQoJy5jb2x1bW4tZnVsbC02LmNvbHVtbi1mdWxsLWltZycpLmhlaWdodCggJ2F1dG8nICk7XG5cbi8vIGVvZiBtYWluIHBhZ2VcblxuLy8gY2FyZCBwYWdlXG4gICAgY29uc3QgdG9nZ2xlRG9nc0luZm8gPSAoKSA9PiB7XG4gICAgICAgICQoJy5jYXJkX19kb2dzIC5tb3JlJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCh0aGlzKS5wYXJlbnRzKCcuY2FyZF9fZG9ncy13cmFwJykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcbiAgICAgICAgICAgICQodGhpcykudGV4dCgpID09PSAn0J/QvtC00YDQvtCx0L3QtdC1JyA/XG4gICAgICAgICAgICAgICAgJCh0aGlzKS50ZXh0KCfQodC60YDRi9GC0YwnKSA6XG4gICAgICAgICAgICAgICAgJCh0aGlzKS50ZXh0KCfQn9C+0LTRgNC+0LHQvdC10LUnKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICB0b2dnbGVEb2dzSW5mbygpO1xuXG4gICAgY29uc3QgaW5pdFNpdHRlclNsaWRlcnMgPSAoKSA9PiB7XG4gICAgICAgICQoJyNzaXR0ZXJNYWluU2xpZGVyJykuc2xpY2soe1xuICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxuICAgICAgICAgICAgYXNOYXZGb3I6ICcjc2l0dGVyTmF2U2xpZGVyJ1xuICAgICAgICB9KTtcbiAgICAgICAgJCgnI3NpdHRlck5hdlNsaWRlcicpLnNsaWNrKHtcbiAgICAgICAgICAgIHNsaWRlc1RvU2hvdzogNSxcbiAgICAgICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgICAgICAgYXNOYXZGb3I6ICcjc2l0dGVyTWFpblNsaWRlcicsXG4gICAgICAgICAgICBkb3RzOiBmYWxzZSxcbiAgICAgICAgICAgIGZvY3VzT25TZWxlY3Q6IHRydWUsXG4gICAgICAgICAgICByZXNwb25zaXZlOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiA3NjgsXG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogNTc2LFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAzLFxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICB9KTtcbiAgICAgICAgJCgnLnNsaWNrLW5leHQnKS5odG1sKCc8aSBjbGFzcz1cImZhcyBmYS1hbmdsZS1yaWdodFwiPjwvaT4nKTtcbiAgICB9O1xuICAgIGluaXRTaXR0ZXJTbGlkZXJzKCk7XG4vLyBjYXJkIHBhZ2VcblxuLy8gc2VhcmNoIHBhZ2VcbiAgICBjb25zdCBzaG93QWRkaXRpb25hbFNlYXJjaENvbHVtbiA9ICgpID0+IHtcbiAgICAgICAgJCgnI2FkZGl0aW9uYWxTZWFyY2ggc3BhbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICQodGhpcykucHJldigpLmhhc0NsYXNzKCdmYS1hbmdsZS11cCcpID9cbiAgICAgICAgICAgICAgICAkKHRoaXMpLnByZXYoKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtdXAnKS5hZGRDbGFzcygnZmEtYW5nbGUtZG93bicpIDpcbiAgICAgICAgICAgICAgICAkKHRoaXMpLnByZXYoKS5yZW1vdmVDbGFzcygnZmEtYW5nbGUtZG93bicpLmFkZENsYXNzKCdmYS1hbmdsZS11cCcpO1xuICAgICAgICAgICAgJCgnLnNlYXJjaF9fY29sdW1uLXRvZ2dsZScpLnNsaWRlVG9nZ2xlKCk7XG4gICAgICAgICAgICAkKHRoaXMpLnRleHQoKSA9PT0gJ9Cg0LDRgdGI0LjRgNC10L3QvdGL0Lkg0L/QvtC40YHQuicgP1xuICAgICAgICAgICAgICAgICQodGhpcykudGV4dCgn0KHQutGA0YvRgtGMJykgOlxuICAgICAgICAgICAgICAgICQodGhpcykudGV4dCgn0KDQsNGB0YjQuNGA0LXQvdC90YvQuSDQv9C+0LjRgdC6Jyk7XG4gICAgICAgIH0pXG4gICAgfTtcbiAgICBzaG93QWRkaXRpb25hbFNlYXJjaENvbHVtbigpO1xuXG5cbiAgICAvLyBqcXVlcnktVUkgZm9yIHNlYXJjaCBibG9ja1xuICAgICQoIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgICQoJy5zZWFyY2hfX2Zvcm0gc2VsZWN0JykubmljZVNlbGVjdCgpO1xuXG4gICAgICAgICQoIFwiLnNlYXJjaF9fZm9ybSBpbnB1dC5zZWFyY2hfX2lucHV0XCIpLmNoZWNrYm94cmFkaW8oKTtcblxuICAgICAgICAkKCBcIiNwcmljZVJhbmdlXCIgKS5zbGlkZXIoe1xuICAgICAgICAgICAgcmFuZ2U6IHRydWUsXG4gICAgICAgICAgICBtaW46IDAsXG4gICAgICAgICAgICBtYXg6IDUwMCxcbiAgICAgICAgICAgIHZhbHVlczogWyA3NSwgMzAwIF0sXG4gICAgICAgICAgICBzbGlkZTogZnVuY3Rpb24oIGV2ZW50LCB1aSApIHtcbiAgICAgICAgICAgICAgICAkKCBcIiNwcmljZUlucHV0XCIgKS52YWwoIFwiJFwiICsgdWkudmFsdWVzWyAwIF0gKyBcIiAtICRcIiArIHVpLnZhbHVlc1sgMSBdICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICAkKCBcIiNwcmljZUlucHV0XCIgKS52YWwoIFwiJFwiICsgJCggXCIjcHJpY2VSYW5nZVwiICkuc2xpZGVyKCBcInZhbHVlc1wiLCAwICkgK1xuICAgICAgICAgICAgXCIgLSAkXCIgKyAkKCBcIiNwcmljZVJhbmdlXCIgKS5zbGlkZXIoIFwidmFsdWVzXCIsIDEgKSApO1xuXG5cbiAgICAgICAgbGV0IGRhdGVGb3JtYXQgPSBcIm1tL2RkL3l5XCIsXG4gICAgICAgICAgICBmcm9tID0gJCggXCIjZGF0ZUZyb21cIiApXG4gICAgICAgICAgICAgICAgLmRhdGVwaWNrZXIoe1xuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0RGF0ZTogXCIrMXdcIixcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlTW9udGg6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIG51bWJlck9mTW9udGhzOiAxXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAub24oIFwiY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB0by5kYXRlcGlja2VyKCBcIm9wdGlvblwiLCBcIm1pbkRhdGVcIiwgZ2V0RGF0ZSggdGhpcyApICk7XG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICB0byA9ICQoIFwiI2RhdGVUb1wiICkuZGF0ZXBpY2tlcih7XG4gICAgICAgICAgICAgICAgZGVmYXVsdERhdGU6IFwiKzF3XCIsXG4gICAgICAgICAgICAgICAgY2hhbmdlTW9udGg6IHRydWUsXG4gICAgICAgICAgICAgICAgbnVtYmVyT2ZNb250aHM6IDFcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLm9uKCBcImNoYW5nZVwiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgZnJvbS5kYXRlcGlja2VyKCBcIm9wdGlvblwiLCBcIm1heERhdGVcIiwgZ2V0RGF0ZSggdGhpcyApICk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3QgZ2V0RGF0ZSA9ICggZWxlbWVudCApID0+IHtcbiAgICAgICAgICAgIGxldCBkYXRlO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBkYXRlID0gJC5kYXRlcGlja2VyLnBhcnNlRGF0ZSggZGF0ZUZvcm1hdCwgZWxlbWVudC52YWx1ZSApO1xuICAgICAgICAgICAgfSBjYXRjaCggZXJyb3IgKSB7XG4gICAgICAgICAgICAgICAgZGF0ZSA9IG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBkYXRlO1xuICAgICAgICB9XG4gICAgfSApO1xuICAgIC8vIEVPRiBqcXVlcnktVUkgZm9yIHNlYXJjaCBibG9ja1xuXG4vLyBlb2Ygc2VhcmNoIHBhZ2VcblxuLy8gcmVnaXN0ZXIgcGFnZVxuICAgICQoIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcucmVnLWZvcm0gc2VsZWN0JykubmljZVNlbGVjdCgpO1xuICAgICAgICAkKCBcIi5yZWctZm9ybSBpbnB1dC5yZWctY2hlY2tib3hcIikuY2hlY2tib3hyYWRpbygpO1xuXG4gICAgICAgICQoJyNzZXRBZGRyZXNzJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSlcbiAgICB9KTtcblxuICAgIGNvbnN0IHNob3dVcGxvYWRlZEltZyA9ICgpID0+IHtcbiAgICAgICAgJCgnaW5wdXRbdHlwZT1cImZpbGVcIl0nKS5vbignY2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coICQodGhpcykudmFsKCkgKTsgLy8gVE9ETzogY2hhbmdlIGRlZmF1bHQgaW1nIGFmdGVyIHVwbG9hZGluZ1xuICAgICAgICB9KVxuICAgIH07XG4gICAgc2hvd1VwbG9hZGVkSW1nKCk7XG5cbi8vIGVvZiByZWdpc3RlciBwYWdlXG5cbiAgICAvLyBtYXNrcyBmb3IgaW5wdXRcbiAgICAkKCcjc2VyaWVzJykubWFzaygnQUEtMDAwMDAwJyk7XG4gICAgJCgnI2lzc3VlX2RhdGUnKS5tYXNrKCcwMC8wMC8wMDAwJyk7XG4gICAgJCgnI2JpcnRoZGF5JykubWFzaygnMDAvMDAvMDAwMCcpO1xuXG4gICAgJCgnLmpzLWF1dG9oZWlnaHQnKS5vbigna2V5dXAnLGZ1bmN0aW9uKCl7XG4gICAgICAgIGxldCBoZWlnaHQgPSB0aGlzLnNjcm9sbEhlaWdodDtcbiAgICAgICAgbGV0IGVsSGVpZ2h0ID0gJCgnLmpzLWF1dG9oZWlnaHQnKS5oZWlnaHQoKTtcbiAgICAgICAgY29uc29sZS5sb2coZWxIZWlnaHQpO1xuICAgICAgICBpZiAoaGVpZ2h0ID4gZWxIZWlnaHQpIHtcbiAgICAgICAgICAgICQodGhpcykuY3NzKCdoZWlnaHQnLCdhdXRvJyk7XG4gICAgICAgICAgICAkKHRoaXMpLmhlaWdodCh0aGlzLnNjcm9sbEhlaWdodCk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIGNvbnRlbnQtcGFnZVxuICAgIGNvbnN0IGFuaW1hdGVTY3JvbGwgPSAoKSA9PiB7XG4gICAgICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkKFwiLmNvbnRlbnQtcGFnZVwiKS5vbihcImNsaWNrXCIsIFwiYVwiLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICBsZXQgaWQgID0gJCh0aGlzKS5hdHRyKCdocmVmJyksXG4gICAgICAgICAgICAgICAgICAgIHRvcCA9ICQoaWQpLm9mZnNldCgpLnRvcDtcbiAgICAgICAgICAgICAgICAkKCdib2R5LGh0bWwnKS5hbmltYXRlKHtzY3JvbGxUb3A6IHRvcH0sIDE1MDApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgYW5pbWF0ZVNjcm9sbCgpO1xuICAgIC8vIGVvZiBjb250ZW50LXBhZ2Vcbn0pO1xuIl0sImZpbGUiOiJhcHBsaWNhdGlvbi5qcyJ9
